/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React, {useState} from 'react';
 import { StyleSheet, TextInput, Text, SafeAreaView, ScrollView, StatusBar, View, Image, Switch, TouchableWithoutFeedback } from 'react-native';
 
 const App = () => {
  const [text, onChangeText] = React.useState(null);
  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch= () =>setIsEnabled(previousState=> !previousState);
  const [count, setCount] = useState(0);
  const onPress= () =>{
    setCount(count+ 1) ;
  };
   return (
     <SafeAreaView style={styles.container}>
       <ScrollView style={styles.scrollView}>
       <View>
       <Text style={styles.title}>Sport News</Text>
        <Image style={styles.img}
          source={{
            uri: 'https://cdn.vox-cdn.com/thumbor/dmRJ1GSSTkCOuHXIzsfQCW2Ww8U=/0x0:3350x2100/920x0/filters:focal(0x0:3350x2100):format(webp):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22951969/1348595156.jpg',
          }}
        />
        <Text style={styles.sub}>Roma played well but werent able to topple Napoli this evening, though they are the first team to get ANYTHING off Napoli this season. 
        </Text>
      </View>
         <Text style={styles.text}>
         Despite all the gloom descending on Trigoria over the past several days, Roma entered their round nine fixture against Napoli in pretty decent shape: 15 points after eight matches, good enough for a tenuous grasp on fourth place. But, with both Lazio and Atalanta dropping points earlier today, Roma had a chance to put a bit of distance between themselves and two of their chief rivals for Serie A final Champions League place next season
         </Text>
        <TextInput
        style={styles.input}
        onChangeText={onChangeText}
        value={text}
        placeholder="Write comment"
        />
        <View style={styles.contain}>
          <Switch
          trackColor={{ false:"white", true:"yellow"}}
          thumbColor={isEnabled? "black": "black"}
          onValueChange={toggleSwitch}
          value={isEnabled}
        />
        </View>
        <View style={styles.count}>
          <Text style={styles.countText}>Count: {count}</Text>
        </View>
        <TouchableWithoutFeedback onPress={onPress}>
         <View style={styles.button}>
           <Text style={styles.TouchText}>Touch Here</Text>
           </View>
        </TouchableWithoutFeedback>
       </ScrollView>
     </SafeAreaView>
   );
 }
 
 const styles = StyleSheet.create({
   container: {
     flex: 1,
     justifyContent: "center",
     paddingTop: StatusBar.currentHeight,
   },
   scrollView: {
     backgroundColor: 'gray',
     marginHorizontal: 15,
   },
   text: {
     fontSize: 35,
     color : 'black',
   },
   sub: {
     fontSize: 15,
     color : 'black',
   },
   title: {
    marginTop: 16,
    paddingVertical: 8,
    borderWidth: 4,
    borderColor: "#20232a",
    borderRadius: 6,
    backgroundColor: "#61dafb",
    color: "#20232a",
    textAlign: "center",
    fontSize: 30,
    fontWeight: "bold"
  },
  img: {
    width: 350,
    height: 200, 
    alignSelf:'center', 
    margin: 20,
  },
  input: {
    height: 100,
    margin: 20,
    borderWidth: 1,
    padding: 10,
  },
  contain: {
    flex: 1,
    alignItems:"center",
    justifyContent: "center",
  },
  count: {
    alignItems:"center",
    padding : 10,
  },
  button:{
    alignItems:"center",
    backgroundColor:"red",
    padding:10
  },
  TouchText:{
    color:"black"
  },
  countText:{
    color:"white"
  },
 });
 
 export default App;
